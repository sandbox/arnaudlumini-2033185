<?php

/*
 * @file
 * Administration form
 */
function relation_interval_settings_form() {
  $form = array();

  $form['relation_interval_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Interval'),
    '#description' => t('The minimum interval between two relations, in seconds. Enter -1 for infinite interval.'),
    '#default_value' => variable_get('relation_interval_interval', '0'),
    '#size' => 20,
    '#required' => TRUE,
    '#element_validate' => array(
      'relation_interval_interval_validate'
    ),
  );

  return system_settings_form($form);
}

function relation_interval_interval_validate($element) {
  if (is_numeric($element['#value'])) {
    return;
  }
  form_error($element, t('The minimum interval must be numeric.'));
}
