<?php

class RelationIntervalAPI {

  private $sourceID;
  private $targetID;
  private $sourceType;
  private $targetType;
  private $relationType;

  public function __construct($sourceID, $targetID, $relationType, $sourceType, $targetType) {
    $entity_info = entity_get_info('relation');

    if (!isset($entity_info['bundles'][$relationType])) {
      throw new Exception('Invalid relation type');
    }

    $this->sourceID = $sourceID;
    $this->targetID = $targetID;
    $this->relationType = $relationType;
    $this->sourceType = $sourceType;
    $this->targetType = $targetType;
  }

  /**
   * The main API function of the module.
   * Creates a relation if the interval requirements are met.
   */
  public function createRelation() {
    $activeRelation = $this->getActiveRelation();

    if ($activeRelation === NULL) {
      $endpoints = $this->getEndpoints();
      $relation = relation_create($this->relationType, $endpoints);
      $relation = relation_save($relation);
      return $relation;
    }

    return NULL;
  }

  public function getActiveRelation() {
    $mostRecentRelation = $this->getMostRecentRelation();

    if ($mostRecentRelation) {
      if (variable_get('relation_interval_interval', '0') == -1) {
        return $mostRecentRelation;
      }
      elseif (time() - $mostRecentRelation->created < variable_get('relation_interval_interval', '0')) {
        return $mostRecentRelation;
      }
    }

    return NULL;
  }

  public function getMostRecentRelation() {
    $endpoints = $this->getEndpoints();
    $matches = relation_relation_exists($endpoints, $this->relationType, TRUE);

    $mostRecentRelation = NULL;
    if ($matches) {
      foreach ($matches as $match) {
        $match = relation_load($match->rid);
        if (!$mostRecentRelation || $match->created > $mostRecentRelation->created) {
          $mostRecentRelation = $match;
        }
      }
    }
    return $mostRecentRelation;
  }

  public function getEndpoints() {
    return array(
      $this->generateEndpoint('source'),
      $this->generateEndpoint('target'),
    );
  }

  public function generateEndpoint($endpoint) {
    $endpoint = array(
      'entity_type' => $this->{$endpoint . 'Type'},
      'entity_id' => $this->{$endpoint . 'ID'}
    );
    return $endpoint;
  }
}
